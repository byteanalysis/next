const express = require('express');
const app = express();

const hostname = '127.0.0.1';
const port = 3000;

app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.get('/farmer/:name', function (req, res) {
    if(req.params.name == 'JOAO')
        res.send('[{\
            "document":{\
              "documentNumber":"475.122.829-11"\
            },\
            "name":"JOAO SILVA",\
            "address":{\
             "street":"RUA BERRINI 505 22050401"\
            }\
          }]');
    else
        res.send('Hello World!');
  });

app.listen(port, hostname, function () {
  console.log(`Server is running in http://${hostname}:${port}/`);
});